﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18._3
{
    class Punt
    {
        private double _x;
        private double _y;

        public double X
        {
            get { return _x; }
            set { _x = value; }
        }
        public double Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public Punt()
        {
            this.X = 0;
            this.Y = 0;
        }
        public Punt(double x, double y)
        {
            this.X = x;
            this.Y = y;

        }
        public virtual string Gegevens()
        {
           // return "( " + X + " " + Y + " )";
           return $"({X:F2},{Y:F2})";

        }
    }
}
