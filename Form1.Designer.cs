﻿namespace _18._3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPunt = new System.Windows.Forms.Button();
            this.btnCirkel = new System.Windows.Forms.Button();
            this.btnCilinder = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPunt
            // 
            this.btnPunt.Location = new System.Drawing.Point(94, 62);
            this.btnPunt.Name = "btnPunt";
            this.btnPunt.Size = new System.Drawing.Size(169, 51);
            this.btnPunt.TabIndex = 0;
            this.btnPunt.Text = "Punt";
            this.btnPunt.UseVisualStyleBackColor = true;
            this.btnPunt.Click += new System.EventHandler(this.btnPunt_Click);
            // 
            // btnCirkel
            // 
            this.btnCirkel.Location = new System.Drawing.Point(94, 144);
            this.btnCirkel.Name = "btnCirkel";
            this.btnCirkel.Size = new System.Drawing.Size(169, 51);
            this.btnCirkel.TabIndex = 1;
            this.btnCirkel.Text = "Cirkel";
            this.btnCirkel.UseVisualStyleBackColor = true;
            this.btnCirkel.Click += new System.EventHandler(this.btnCirkel_Click);
            // 
            // btnCilinder
            // 
            this.btnCilinder.Location = new System.Drawing.Point(94, 238);
            this.btnCilinder.Name = "btnCilinder";
            this.btnCilinder.Size = new System.Drawing.Size(169, 51);
            this.btnCilinder.TabIndex = 2;
            this.btnCilinder.Text = "Cilnder";
            this.btnCilinder.UseVisualStyleBackColor = true;
            this.btnCilinder.Click += new System.EventHandler(this.btnCilinder_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(356, 349);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Sluiten";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnCilinder);
            this.Controls.Add(this.btnCirkel);
            this.Controls.Add(this.btnPunt);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPunt;
        private System.Windows.Forms.Button btnCirkel;
        private System.Windows.Forms.Button btnCilinder;
        private System.Windows.Forms.Button button1;
    }
}

