﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18._3
{
    class Cilinder: Cirkel
    {
        private double _h;
        public double H { get; set; }

        public Cilinder() : base()
        {
            this.H = 0;
        }
        public Cilinder(double x, double y, double r, double h) : base(x, y,r)
        {
            this.H = h;
        }
       
        public override string Gegevens()
        {
            return base.Gegevens() + $" straal {R}\r\nOmtrek = {Omtrek():F2}\r\nOpervlakte = {Oppervlakte():f2}";
        }
        public override double Oppervlakte()
        {
            return (2 * base.Oppervlakte()) + (base.Omtrek() * H);
        }
        public double Volume()
        {
           return  base.Oppervlakte() * H;
        }
    }
}
