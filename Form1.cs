﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _18._3
{
    public partial class Form1 : Form
    {
        Punt p = new Punt();
        Cirkel ck = new Cirkel();
        Cilinder cl = new Cilinder();
        public Form1()
        {
            p = new Punt(3, 4);         
            ck = new Cirkel(p.X, p.Y, 12);
            cl = new Cilinder(ck.X, ck.Y, ck.R, 8);
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPunt_Click(object sender, EventArgs e)
        {
            MessageBox.Show(p.Gegevens());
        }

        private void btnCirkel_Click(object sender, EventArgs e)
        {
            MessageBox.Show(ck.Gegevens());
        }

        private void btnCilinder_Click(object sender, EventArgs e)
        {
            MessageBox.Show(cl.Gegevens());
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
