﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18._3
{
    class Cirkel : Punt
    {
        private double _r;
        public double R { get; set; }

        public Cirkel():base()
        {
            this.R = 0;
        }
        public Cirkel(double x, double y, double r) : base(x, y)
        {
            R = r;
        }
     
        public double Omtrek()
        {
            return 2 * Math.PI * R;

        }
        public virtual double Oppervlakte()
        {           
            return Math.PI* Math.Pow(R, 2);
        }
        public override string Gegevens()
        {
            return base.Gegevens() + $" straal {R}\r\nOmtrek = {Omtrek():F2}\r\nOpervlakte = {Oppervlakte():f2}";

        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
